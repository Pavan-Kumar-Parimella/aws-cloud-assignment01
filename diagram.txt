                 +----------------------------------+
                 |           AWS Account            |
                 +----------------------------------+
                                |
                                |
                +----------------------------------+
                |         Virtual Private Cloud      |
                |          (VPC - 10.0.0.0/16)       |
                +----------------------------------+
                        |                 |
        +---------------------------------------+
        |              Subnet 1 (Public)         |
        |          (10.0.1.0/28 - 16 IPs)        |
        |          NACL: Public NACL             |
        |          SG: Public Security Group     |
        +---------------------------------------+
                        |                 |
        +---------------------------------------+
        |              Subnet 2 (Private)        |
        |          (10.0.2.0/28 - 16 IPs)        |
        |          NACL: Private NACL            |
        |          SG: Private Security Group    |
        +---------------------------------------+
                        |                 |
        +---------------------------------------+
        |              EC2 Instance              |
        |       (Subnet: Subnet 1 - Public)      |
        |    Security Group: Public Security Group|
        |              Code Deployment           |
        +---------------------------------------+
                        |                 |
        +---------------------------------------+
        |               RDS Database             |
        |         (Private channel access)       |
        +---------------------------------------+
                                |
                                |
                +----------------------------------+
                |         Elastic Beanstalk         |
                |            Application           |
                +----------------------------------+
                                |
                                |
                +----------------------------------+
                |              Lambda              |
                |   (Triggered by S3 File Upload)   |
                +----------------------------------+
                                |
                                |
                +----------------------------------+
                |               S3 Bucket           |
                +----------------------------------+
                 +----------------------------------+
                 |           AWS Account            |
                 +----------------------------------+
                                |
                                |
                +----------------------------------+
                |         Virtual Private Cloud      |
                |          (VPC - 10.0.0.0/16)       |
                +----------------------------------+
                        |                 |
        +---------------------------------------+
        |              Subnet 1 (Public)         |
        |          (10.0.1.0/28 - 16 IPs)        |
        |          NACL: Public NACL             |
        |          SG: Public Security Group     |
        +---------------------------------------+
                        |                 |
        +---------------------------------------+
        |              Subnet 2 (Private)        |
        |          (10.0.2.0/28 - 16 IPs)        |
        |          NACL: Private NACL            |
        |          SG: Private Security Group    |
        +---------------------------------------+
                        |                 |
        +---------------------------------------+
        |              EC2 Instance              |
        |       (Subnet: Subnet 1 - Public)      |
        |    Security Group: Public Security Group|
        |              Code Deployment           |
        +---------------------------------------+
                        |                 |
        +---------------------------------------+
        |               RDS Database             |
        |         (Private channel access)       |
        +---------------------------------------+
                                |
                                |
                +----------------------------------+
                |         Elastic Beanstalk         |
                |            Application           |
                +----------------------------------+
                                |
                                |
                +----------------------------------+
                |              Lambda              |
                |   (Triggered by S3 File Upload)   |
                +----------------------------------+
                                |
                                |
                +----------------------------------+
                |               S3 Bucket           |
                +----------------------------------+

This architecture diagram illustrates the following components and their relationships:

AWS Account: Represents the AWS account used for the project.

Virtual Private Cloud (VPC): The VPC is the networking foundation for the project, providing isolated virtual network environments.

Subnet 1 (Public): A public subnet with a range of 16 IPs (10.0.1.0/28). This subnet allows inbound and outbound internet traffic.

Subnet 2 (Private): A private subnet with a range of 16 IPs (10.0.2.0/28). This subnet restricts inbound internet traffic.

EC2 Instance: A virtual machine deployed in Subnet 1 (Public) running the application code. It communicates with the RDS database via the private channel.

RDS Database: Represents the database instance (RDS) that the application code communicates with. It is accessible only through the private channel.

Elastic Beanstalk: AWS Elastic Beanstalk is used to deploy the application code in an auto-scaling and managed environment.

Lambda: A serverless function triggered by file uploads to the S3 bucket. It prints the name of the uploaded file.

S3 Bucket: Amazon S3 bucket used for file storage and triggering the Lambda function upon file upload.